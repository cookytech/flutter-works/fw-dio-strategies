/// Class that we will use to bridge key value stores with different 
/// types of stores like sqlite, hive and 
abstract class KeyValueStore {
  Object get(String key);
  Future<bool> set(String key, Object value);
}
