import 'package:dio/dio.dart';

class LoggingInterceptor implements Interceptor {
  @override
  Future onError(DioError err) async{
    print(err);
    return err;
  }

  @override
  Future onRequest(RequestOptions options) async {
    print('request $options');
    return options;
  }

  @override
  Future onResponse(Response response) async {
    print('response $response');
    return response;
  }
}
