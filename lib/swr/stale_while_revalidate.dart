import 'package:dio/dio.dart';

enum SWREventStatus{error, }

class SWREvent {
  
}

extension StaleWhileRevalidate on Dio {
  /// This function wraps [Dio.getUri] with a chaching mechanism.
  /// Caching strategy is to use uri as key
  Stream getSWRUri() async* {}

  /// This function wraps [Dio.get] with a caching mechanism.
  /// Caching strategy is to use path + hash(queryParameters) as the key
  Stream getSWR() async* {}
}
