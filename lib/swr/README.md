# Stale While Relavidate

Inspired by workbox's stale while revalidate strategy. Link: https://developers.google.com/web/tools/workbox/modules/workbox-strategies#stale-while-revalidate 
This will:

1. Serve the response from local cache (stale response)
2. Fetch a new response from internet
3. Compare response from the local cahce, if equal, do nothing
4. If new response is different from the local response, it will update the cache and revalidate the response to the service / caller.

HTTP Method Support Matrix

__________________________
| method | is supported |
__________________________
| http.get | yes |
| others   | no  |
____________________


We will:  
* try to keep the syntax really simple, 
* try to follow decorator, 
* try to implement our strategies within our codebase instead of depending too much on functionality provided by dio itself. So we could also be compatible regardless of whether anyone has applied external interceptors etc. 

Just import the library and use extension methods on dio client to your satistaction

```
StreamSubscription subscription = client.getSWR("request_string").listen();

```


## Technical Diagrams:

Timeline and stream events:

![Timeline and Stream Events](https://gitlab.com/cookytech/flutter-works/fw-dio-strategies/-/blob/master/technical_diagrams/swr/swr_fsm_stream_event_timeline.png)