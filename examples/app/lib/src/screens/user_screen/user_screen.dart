import 'package:app/src/models/user_data.dart';
import 'package:app/src/services/user_service.dart';
import 'package:app/src/stores/user_store.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  UserStore userStore;

  @override
  void initState() {
    super.initState();
    userStore = UserStore(userService: UserService());
    userStore.getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('username'),
      ),
      body: Observer(
        builder: (_) => userStore.hasInitialized
            ? Text(
                userStore.users
                    .map((UserData user) => user.toJson().toString())
                    .toList()
                    .reduce((value, element) => '$value \n $element'),
              )
            : Text('loading...'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print('ON_FAB_PRESSED');
          Dio()
              .get('https://reqres.in/api/users?page=1')
              .then((response) => print(response.statusCode));
        },
      ),
    );
  }
}
