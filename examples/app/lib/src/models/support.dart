import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'support.g.dart';

@JsonSerializable()
class Support extends Equatable {
	@JsonKey(name: 'url')
	final String url;
	@JsonKey(name: 'text')
	final String text;

	const Support({this.url, this.text});

	factory Support.fromJson(Map<String, dynamic> json) {
		return _$SupportFromJson(json);
	}

	Map<String, dynamic> toJson() => _$SupportToJson(this);

	@override
	List<Object> get props => [url, text];
}
