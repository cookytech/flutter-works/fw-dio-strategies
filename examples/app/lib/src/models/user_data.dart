import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_data.g.dart';

@JsonSerializable()
class UserData extends Equatable {
	@JsonKey(name: 'id')
	final int id;
	@JsonKey(name: 'email')
	final String email;
	@JsonKey(name: 'first_name')
	final String firstName;
	@JsonKey(name: 'last_name')
	final String lastName;
	@JsonKey(name: 'avatar')
	final String avatar;

	const UserData({
		this.id,
		this.email,
		this.firstName,
		this.lastName,
		this.avatar,
	});

	factory UserData.fromJson(Map<String, dynamic> json) => _$UserDataFromJson(json);

	Map<String, dynamic> toJson() => _$UserDataToJson(this);

	@override
	List<Object> get props => [id, email, firstName, lastName, avatar];
}
