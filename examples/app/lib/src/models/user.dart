import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import "user_data.dart";
import "support.dart";

part 'user.g.dart';

@JsonSerializable()
class User extends Equatable {
	@JsonKey(name: 'page')
	final int page;
	@JsonKey(name: 'per_page')
	final int perPage;
	@JsonKey(name: 'total')
	final int total;
	@JsonKey(name: 'total_pages')
	final int totalPages;
	@JsonKey(name: 'data')
	final List<UserData> data;
	@JsonKey(name: 'support')
	final Support support;

	const User({
		this.page,
		this.perPage,
		this.total,
		this.totalPages,
		this.data,
		this.support,
	});

	factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

	Map<String, dynamic> toJson() => _$UserToJson(this);

	@override
	List<Object> get props {
		return [
			page,
			perPage,
			total,
			totalPages,
			data,
			support,
		];
	}
}
