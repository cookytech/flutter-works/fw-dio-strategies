import 'package:app/src/models/user.dart';
import 'package:app/src/models/user_data.dart';
import 'package:dio/dio.dart';
import 'package:fw_dio_strategies/fw_dio_strategies.dart';

class UserService {
  Future<List<UserData>> getUsers() async {
    List<UserData> users;
    try {
      final client = Dio()
      ..interceptors.add(LoggingInterceptor());
      final response = await client.get('https://reqres.in/api/users?page=1');
      users = User.fromJson(response.data).data;
    } catch (e) {
      print(e);
    } finally {}
    return users;
  }
}
