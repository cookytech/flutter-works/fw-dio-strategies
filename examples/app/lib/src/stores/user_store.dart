import 'package:app/src/models/user_data.dart';
import 'package:app/src/services/user_service.dart';
import 'package:meta/meta.dart';
import 'package:mobx/mobx.dart';
part 'user_store.g.dart';

/// making sure that the service is injected to the store
class UserStore extends _UserStore with _$UserStore {
  UserStore({@required UserService userService})
      : super(userService: userService);
}

abstract class _UserStore with Store {
  final UserService userService;

  _UserStore({@required this.userService});

  @observable
  List<UserData> users;

  @computed
  bool get hasInitialized => users != null;

  @action
  getUsers() async {
    users = await userService.getUsers();
  }
}
