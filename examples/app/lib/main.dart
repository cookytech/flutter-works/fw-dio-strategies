import 'package:flutter/material.dart';

import 'src/screens/home_screen/home_screen.dart';
import 'src/screens/user_screen/user_screen.dart';

void main() {
  runApp(MyApp());
}

final Map<String, Widget Function(BuildContext)> routeMap = {
  '/': (_) => HomeScreen(), // will show a list of users
  '/user': (_) => UserScreen(), // will show the specific user 
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (BuildContext context, Widget child) => Scaffold(
        /// This builder ensures that there is an app level scaffold for other
        /// scaffolds to be nested in so no screen is without a scaffold
        body: child,
      ),
      routes: routeMap,
      initialRoute: '/user',
    );
  }
}
